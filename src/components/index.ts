import { ShowHideContainer } from './show-hide-password/show-hide-container';
import { ShowHideInput } from './show-hide-password/show-hide-input';
import { COMPONENTS_MAIN } from './main-onawa/component';

export const COMPONENTS: Array<any> = [
  ShowHideContainer,
  ShowHideInput,
  COMPONENTS_MAIN
];
