import { Component, EventEmitter, Input, Output } from '@angular/core';

/*
 Generated class for the Login component.

 See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 for more info on Angular 2 Components.
 */
@Component({
    selector: 'main-onawa-index',
    templateUrl: 'index.html'
})
export class MainOnawaIndexComponent {

    @Input('options')
    options: any;

    @Output('addLoginEvent')
    addLoginEvent = new EventEmitter<any>();

    @Output('addResetPasswordEvent')
    addResetPasswordEvent = new EventEmitter<any>();

    @Output('addInviteEvent')
    addInviteEvent = new EventEmitter<any>();

    constructor() {}

    showPopupLogin() {
        this.addLoginEvent.emit(this);
    }

    showPopupResetPassword() {
        this.addResetPasswordEvent.emit(this);
    }

    showPopupInviteCode() {
        this.addInviteEvent.emit(this);
    }
}
