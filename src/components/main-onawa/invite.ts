import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';

/*
 Generated class for the Login component.

 See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 for more info on Angular 2 Components.
 */
@Component({
    selector: 'main-onawa-invite',
    templateUrl: 'invite.html'
})
export class MainOnawaInviteComponent {

    form: FormGroup;

    @Input('options')
    options: any;

    @Output('addLoginEvent')
    addLoginEvent = new EventEmitter<any>();

    constructor(
        formBuilder: FormBuilder,
        public navCtrl: NavController,
        public navParams: NavParams
    ) {

        this.form = formBuilder.group({
            code: [ '', Validators.compose([ Validators.required ]) ],
        });
    }

    close() {
        this.navCtrl.pop();
    }

}
