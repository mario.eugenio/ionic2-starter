import { MainOnawaIndexComponent } from './index';
import { MainOnawaLoginComponent } from './login';
import { MainOnawaResetPasswordComponent } from './reset-password';
import { MainOnawaInviteComponent } from 'invite';

export const COMPONENTS_MAIN: Array<any> = [
    MainOnawaLoginComponent,
    MainOnawaIndexComponent,
    MainOnawaResetPasswordComponent,
    MainOnawaInviteComponent,
];
