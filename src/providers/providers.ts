import { ERROR_HANDLER } from './error.handler';
import { User } from './user';

export const Providers: any[] = [
    ERROR_HANDLER,
    User,
];

export { User };
