import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'page-userlist',
    templateUrl: 'user-list.html',
    providers: [  ]
})
export class UserListPage implements OnInit {

    constructor(public navCtrl: NavController) {
    }

    /**
     * Exemplo de  Navegação passando a Model para a página seguinte
     * @param user
     */
    detalharUsuario() {

    }

    ngOnInit(): void {

    }
}
