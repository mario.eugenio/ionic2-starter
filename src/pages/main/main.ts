import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RecoveryPasswordPage } from '../recovery-password/recovery-password';
import { InvitePage } from '../invite/invite';

/*
  Generated class for the Main page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-main',
  templateUrl: 'main.html'
})
export class MainPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {}

  showLogin() {
    this.navCtrl.push(LoginPage);
  }

  showResetPassword() {
    this.navCtrl.push(RecoveryPasswordPage);
  }

  showInvite() {
    this.navCtrl.push(InvitePage);
  }
}
